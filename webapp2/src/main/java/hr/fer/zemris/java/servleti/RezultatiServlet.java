package hr.fer.zemris.java.servleti;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.servleti.RezultatiServlet.BendIBrojGlasova;

/**
 * Class which represents servlet to show results of given votes from users. It
 * is connected to glasanjeRez.jsp where with it cooperates and shows to user
 * table of votes for every band, pie chart how does it look visually, excel
 * file where are stored votes and bends, and URL for winners bands song
 * 
 * @author Miroslav Filipovic
 */
@WebServlet(name = "glasanje-rezultati", urlPatterns = { "/glasanje-rezultati" })
public class RezultatiServlet extends HttpServlet {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String fileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");
		int max = 0;
		ArrayList<BendIBrojGlasova> songs = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(Files.newInputStream(Paths.get(fileName))))) {
			while (true) {
				String song = br.readLine();
				if (song == null || song.trim().isEmpty()) {
					break;
				}
				BendIBrojGlasova songa = new BendIBrojGlasova(song, req);
				songs.add(songa);
				if (max < Integer.parseInt(songa.getGlasovi())) {
					max = Integer.parseInt(songa.getGlasovi());
				}
			}
		} catch (Exception e) {
		}

		ArrayList<String> pobjednici = new ArrayList<>();
		for (BendIBrojGlasova b : songs) {
			if (Integer.parseInt(b.getGlasovi()) >= max) {
				pobjednici.add(b.getBend());
			}
		}

		ArrayList<BendIUrl> pobjedniciBendIUrl = new ArrayList<>();
		for (String b : pobjednici) {
			pobjedniciBendIUrl.add(new BendIUrl(b, req));
		}
		
		Collections.sort(songs);
		req.setAttribute("bendiglasovi", songs);
		req.setAttribute("pobjednici", pobjedniciBendIUrl);

		req.getRequestDispatcher("/WEB-INF/pages/glasanjeRez.jsp").forward(req, resp);
	}

	/**
	 * Class that is used so we can store name of bend and number of votes so we
	 * can display it to user over glasanjeRez.jsp file
	 * 
	 * @author Miroslav Filipovic
	 */
	public static class BendIBrojGlasova implements Comparable<BendIBrojGlasova> {

		/**
		 * Name of bend
		 */
		private String bend;

		/**
		 * Votes of bend
		 */
		private String glasovi;

		/**
		 * Constructor which is used to make new instance of this class with
		 * given string, which has informations of bends name and how many votes
		 * does it have
		 * 
		 * @param bend
		 *            name of bend and number of votes
		 * @param req
		 *            servlet request that we need to access results
		 */
		public BendIBrojGlasova(String bend, HttpServletRequest req) {
			String fileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt");
			try (BufferedReader br = new BufferedReader(
					new InputStreamReader(Files.newInputStream(Paths.get(fileName))))) {
				while (true) {
					String song = br.readLine();
					if (song == null || song.trim().isEmpty()) {
						break;
					}
					if (song.split("\t")[0].equals(bend.split(" ")[0])) {
						this.bend = song.split("\t")[1];
					}
				}
			} catch (Exception e) {
			}
			String[] parts = bend.split(" ");
			this.glasovi = parts[1];
		}

		/**
		 * Getter for bend name
		 * 
		 * @return name of bend
		 */
		public String getBend() {
			return bend;
		}

		/**
		 * Setter for bend name
		 * 
		 * @param bend
		 *            name of bend
		 */
		public void setBend(String bend) {
			this.bend = bend;
		}

		/**
		 * Getter for votes
		 * 
		 * @return number of votes
		 */
		public String getGlasovi() {
			return glasovi;
		}

		/**
		 * Setter for votes
		 * 
		 * @param glasovi
		 *            number of votes
		 */
		public void setGlasovi(String glasovi) {
			this.glasovi = glasovi;
		}

		@Override
		public int compareTo(BendIBrojGlasova o) {
			return Long.compare(Long.parseLong(o.glasovi), Long.parseLong(this.glasovi));
		}

	}

	/**
	 * Class that is used to represent band and URL over which we can access
	 * their song so we can in file glasanjeRez.jsp make link to user over that
	 * URL and show name of band which song that is
	 * 
	 * @author Miroslav Filipovic
	 */
	public static class BendIUrl {

		/**
		 * Name of band
		 */
		private String bend;

		/**
		 * URL of bands song
		 */
		private String url;

		/**
		 * Constructor that is used to make new instance of this class where is
		 * needed bend parameter over which we find name of band and url that we
		 * need for that band
		 * 
		 * @param bend
		 *            where is stored name and url
		 * @param req
		 *            over which we can get file glasanje-definicija.txt
		 */
		public BendIUrl(String bend, HttpServletRequest req) {
			String fileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt");
			try (BufferedReader br = new BufferedReader(
					new InputStreamReader(Files.newInputStream(Paths.get(fileName))))) {
				while (true) {
					String song = br.readLine();
					if (song == null || song.trim().isEmpty()) {
						break;
					}
					if (bend.equals(song.split("\t")[1])) {
						this.bend = song.split("\t")[1];
						this.url = song.split("\t")[2];
					}
				}
			} catch (Exception e) {
			}
		}

		/**
		 * Getter for bend name
		 * 
		 * @return name of band
		 */
		public String getBend() {
			return bend;
		}

		/**
		 * Setter for band name
		 * 
		 * @param bend
		 *            name of band
		 */
		public void setBend(String bend) {
			this.bend = bend;
		}

		/**
		 * Getter for band URL
		 * 
		 * @return bands URL
		 */
		public String getUrl() {
			return url;
		}

		/**
		 * Setter for band URL
		 * 
		 * @param url
		 *            of band
		 */
		public void setUrl(String url) {
			this.url = url;
		}

	}

}