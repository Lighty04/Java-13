package hr.fer.zemris.java.servleti;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class which represent servlet that is used when we want to send to *.jsp file
 * which bands does it need to show to users. It must send data which is then
 * represent as link so user can vote for band they like.
 * 
 * @author Miroslav Filipovic
 */
@WebServlet(name = "glasanje", urlPatterns = { "/glasanje" })
public class GlasanjeServlet extends HttpServlet {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String fileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt");
		ArrayList<Bend> songs = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(Files.newInputStream(Paths.get(fileName))))) {
			while (true) {
				String song = br.readLine();
				if (song == null || song.trim().isEmpty()) {
					break;
				}
				songs.add(new Bend(song));
			}
		} catch (Exception e) {
		}
		req.setAttribute("songs", songs);

		req.getRequestDispatcher("/WEB-INF/pages/glasanjeIndex.jsp").forward(req, resp);
	}

	/**
	 * Class used to represent bend with number on list in file, their name and
	 * url where we can find songs of that band. It only have getters and
	 * setters for these attributes and one public constructor
	 * 
	 * @author Miroslav Filipovic
	 */
	public static class Bend {

		/**
		 * Number where are they in file
		 */
		private String number;

		/**
		 * Name of bend
		 */
		private String name;

		/**
		 * Url which leads to their song
		 */
		private String url;

		/**
		 * Constructor which is used to make instance of this class, as argument
		 * takes one string with 3 informations which are separated with tab
		 * (\t)
		 * 
		 * @param song
		 *            where is all data of band
		 */
		public Bend(String song) {
			super();
			String[] parts = song.split("\t");
			number = parts[0];
			name = parts[1];
			url = parts[2];
		}

		/**
		 * Getter for number
		 * 
		 * @return number of bend
		 */
		public String getNumber() {
			return number;
		}

		/**
		 * Setter for number
		 * 
		 * @param number
		 *            of bend
		 */
		public void setNumber(String number) {
			this.number = number;
		}

		/**
		 * Getter for name
		 * 
		 * @return name of bend
		 */
		public String getName() {
			return name;
		}

		/**
		 * Setter for name
		 * 
		 * @param name
		 *            of bend
		 */
		public void setName(String name) {
			this.name = name;
		}

		/**
		 * Getter for url
		 * 
		 * @return url of bend
		 */
		public String getUrl() {
			return url;
		}

		/**
		 * Setter for url
		 * 
		 * @return url of bend
		 */
		public void setUrl(String url) {
			this.url = url;
		}

	}
}
