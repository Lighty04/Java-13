package hr.fer.zemris.java.servleti;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class that represent servlet which is used to send data for table that is
 * then print out to web application. It takes two parameters, a is start and b
 * is end, and it will calculate all sin and cos for those integer numbers
 * between a and b and send that data to page
 * 
 * @author Miroslav Filipovic
 */
@WebServlet(name = "trigonometric", urlPatterns = { "/trigonometric" })
public class TrigonometricServlet extends HttpServlet {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		int a = 0;
		int b = 0;

		try {
			a = Integer.parseInt(req.getParameter("a"));
		} catch (Exception e) {
			a = 0;
		}
		try {
			b = Integer.parseInt(req.getParameter("b"));
		} catch (Exception e) {
			b = 360;
		}

		if (a > b) {
			a = a + b;
			b = a - b;
			a = a - b;
		}

		if (b > a + 720) {
			b = a + 720;
		}

		ArrayList<String> sin = new ArrayList<>();
		ArrayList<String> cos = new ArrayList<>();

		for (int i = a; i <= b; i++) {
			sin.add(Math.sin(i * Math.PI / 180) + "");
			cos.add(Math.cos(i * Math.PI / 180) + "");
		}

		req.setAttribute("sin", sin);
		req.setAttribute("cos", cos);

		req.getRequestDispatcher("WEB-INF/pages/trigonometric.jsp").forward(req, resp);
	}

}
