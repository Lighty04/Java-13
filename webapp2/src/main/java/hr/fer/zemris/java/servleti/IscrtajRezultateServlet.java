package hr.fer.zemris.java.servleti;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.util.Rotation;

import com.keypoint.PngEncoder;

import hr.fer.zemris.java.servleti.RezultatiServlet.BendIBrojGlasova;

/**
 * Class that is used to print all data as pie chart on web application. It uses
 * data from glasanje-rezultati.txt where are stored all information that this
 * chart need to show us which band has most votes from all bands
 * 
 * @author Miroslav Filipovic
 */
public class IscrtajRezultateServlet extends HttpServlet {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		PieDataset dataset = createDataset(req);
		JFreeChart chart = createChart(dataset, "Rezultati");

		resp.setContentType("image/png");

		BufferedImage buf = chart.createBufferedImage(640, 400, null);
		PngEncoder encoder = new PngEncoder(buf, false, 0, 9);
		resp.getOutputStream().write(encoder.pngEncode());
	}

	/**
	 * Method used to create all data that our chart needs so we can make new
	 * pie chart which we will print out for user
	 * 
	 * @param req
	 *            servlet request that we need for our result file
	 * @return PieDataset where is stored all data that chart needs
	 */
	private PieDataset createDataset(HttpServletRequest req) {

		DefaultPieDataset result = new DefaultPieDataset();

		String fileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");

		ArrayList<BendIBrojGlasova> songs = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(Files.newInputStream(Paths.get(fileName))))) {
			while (true) {
				String song = br.readLine();
				if (song == null || song.trim().isEmpty()) {
					break;
				}
				BendIBrojGlasova songa = new BendIBrojGlasova(song, req);
				songs.add(songa);
			}
		} catch (Exception e) {
		}

		for (BendIBrojGlasova b : songs) {
			result.setValue(b.getBend(), Integer.parseInt(b.getGlasovi()));
		}

		return result;

	}

	/**
	 * Method which is used to make new pie chart with data that we have sent
	 * over arguments
	 * 
	 * @param dataset
	 *            where is data stored
	 * @param title
	 *            how will chart call
	 * @return new instance of chart that we will show to user
	 */
	private JFreeChart createChart(PieDataset dataset, String title) {

		JFreeChart chart = ChartFactory.createPieChart3D(title, // chart title
				dataset, true, true, false);

		PiePlot3D plot = (PiePlot3D) chart.getPlot();
		plot.setStartAngle(290);
		plot.setDirection(Rotation.CLOCKWISE);
		plot.setForegroundAlpha(0.5f);
		return chart;

	}

}
