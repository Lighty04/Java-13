package hr.fer.zemris.java.servleti;

/**
 * Class used to represent time so we can over servlet more easily send user how
 * much time has gone since server has been turned on
 * 
 * @author Miroslav Filipovic
 */
public class Time {

	/**
	 * Current time
	 */
	private String time = "" + System.currentTimeMillis();

	/**
	 * Constructor so we can make new instance every time when we want to find
	 * out how much time has passed
	 */
	public Time() {
	}

	/**
	 * Getter for time in more nicely way as days hours minutes seconds
	 * milliseconds format
	 * 
	 * @return time in way as is written above
	 */
	public String getTime() {
		Long t = Long.parseLong(time);
		String formatedTime = "";
		long days = t / (24 * 60 * 60 * 1000);
		formatedTime += days + " days, ";
		t -= days * (24 * 60 * 60 * 1000);

		long hours = t / (60 * 60 * 1000);
		formatedTime += hours + " hours, ";
		t -= hours * (60 * 60 * 1000);

		long minutes = t / (60 * 1000);
		formatedTime += minutes + " minutes, ";
		t -= minutes * (60 * 1000);

		long seconds = t / (1000);
		formatedTime += seconds + " seconds, ";
		t -= seconds * (1000);

		long miliseconds = t;
		formatedTime += miliseconds + " miliseconds";

		return formatedTime;
	}

	/**
	 * Setter for how much time passed
	 * 
	 * @param time
	 *            when server started
	 */
	public void setTime(String time) {
		this.time = "" + (System.currentTimeMillis() - Long.parseLong(time));
	}

}
