package hr.fer.zemris.java.servleti;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import hr.fer.zemris.java.servleti.RezultatiServlet.BendIBrojGlasova;

/**
 * Class that represent servlet which is used to make new excel file where is
 * stored data. It is written bands name and votes that they are given over web
 * applications from user votes. When user starts this servlet, it will make
 * start download of that file
 * 
 * @author Miroslav Filipovic
 */
public class RezultatiUExcelServlet extends HttpServlet {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			resp.setContentType("application/ms-excel");
			resp.setHeader("Content-Disposition", "attachment; filename=rezultati.xls");

			String fileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");

			ArrayList<BendIBrojGlasova> songs = new ArrayList<>();
			try (BufferedReader br = new BufferedReader(
					new InputStreamReader(Files.newInputStream(Paths.get(fileName))))) {
				while (true) {
					String song = br.readLine();
					if (song == null || song.trim().isEmpty()) {
						break;
					}
					songs.add(new BendIBrojGlasova(song, req));
				}
			} catch (Exception e) {
			}
			
			Collections.sort(songs);

			HSSFWorkbook wb = new HSSFWorkbook();
			HSSFSheet sheet = wb.createSheet();

			int i = 0;
			for (BendIBrojGlasova b : songs) {
				HSSFRow row = sheet.createRow((short) i);
				row.createCell((short) 0).setCellValue(b.getBend());
				row.createCell((short) 1).setCellValue(b.getGlasovi());
				i++;
			}

			ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
			wb.write(outByteStream);

			byte[] outArray = outByteStream.toByteArray();
			OutputStream outStream = resp.getOutputStream();
			outStream.write(outArray);
			outStream.flush();

		} catch (Exception ex) {
		}
	}

}
