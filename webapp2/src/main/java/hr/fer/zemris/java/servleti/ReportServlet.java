package hr.fer.zemris.java.servleti;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class which represent servlet, it is just used to we can with simple name as
 * /reportImage access our page which is report.jsp, so user doesn't need to
 * care about .jsp extension. It just redirects request and response to
 * report.jsp file
 * 
 * @author Miroslav Filipovic
 */
@WebServlet(name = "reportImage", urlPatterns = { "/reportImage" })
public class ReportServlet extends HttpServlet {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("WEB-INF/pages/report.jsp").forward(req, resp);
	}

}
