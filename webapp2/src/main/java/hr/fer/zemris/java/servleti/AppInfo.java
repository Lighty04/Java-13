package hr.fer.zemris.java.servleti;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * This class represents servlet listener which is used when we want to find out
 * when our server started working. As soon as it started working method
 * contextInitialized is called and we set currentTime to milliseconds that
 * passed until that moment. We put that information as attribute so other
 * classes can use it
 * 
 * @author Miroslav Filipovic
 */
@WebListener
public class AppInfo implements ServletContextListener {

	/**
	 * Current time when server started running
	 * */
	protected long currentTime;

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		currentTime = 0;
		arg0.getServletContext().setAttribute("time", currentTime);
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		currentTime = System.currentTimeMillis();
		arg0.getServletContext().setAttribute("time", currentTime);
	}

}
