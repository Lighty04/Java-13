package hr.fer.zemris.java.servleti;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class which is used to implement some job and then redirect response to other
 * servlet. It is used to make change in data which if we want to add one more
 * vote for some band. It takes already stored file called
 * glasanje-rezultati.txt, searches for band, and increments its value for 1
 * 
 * @author Miroslav Filipovic
 */
@WebServlet(name = "glasanje-glasaj", urlPatterns = { "/glasanje-glasaj" })
public class GlasajServlet extends HttpServlet {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String num = (String) req.getParameter("name");
		String fileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");
		String result = "";
		try (BufferedReader br = new BufferedReader(new InputStreamReader(Files.newInputStream(Paths.get(fileName))))) {
			while (true) {
				String song = br.readLine();
				if (song == null || song.trim().isEmpty()) {
					break;
				}
				if (song.split(" ")[0].equals(num)) {
					song = num + " " + (Integer.parseInt(song.split(" ")[1]) + 1);
				}
				result += song + "\n";
			}
		} catch (Exception e) {
		}

		File file = new File(fileName);

		OutputStream os = Files.newOutputStream(file.toPath());

		os.write(result.getBytes());
		os.flush();

		resp.sendRedirect(req.getContextPath() + "/glasanje-rezultati");
	}
}
