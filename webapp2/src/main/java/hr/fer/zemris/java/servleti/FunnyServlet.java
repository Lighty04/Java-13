package hr.fer.zemris.java.servleti;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class used which represent servlet which we is connected with .*jsp in which
 * we write some funny story every time with different letter colour. Colors of
 * letters can be red, green, blue, gray, yellov, purple, brown, black or white.
 * 
 * @author Miroslav Filipovic
 */
@WebServlet(name = "funny", urlPatterns = { "/funny" })
public class FunnyServlet extends HttpServlet {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setAttribute("color", getRandomColor());

		req.getRequestDispatcher("WEB-INF/stories/funny.jsp").forward(req, resp);
	}

	/**
	 * Method used to return some random colour from array of colours
	 * 
	 * @return some colour
	 */
	private String getRandomColor() {
		String[] colors = { "red", "green", "blue", "gray", "yellov", "purple", "brown", "black", "white" };
		return colors[(int) (Math.random() * colors.length)];
	}

}
