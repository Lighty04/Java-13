package hr.fer.zemris.java.servleti;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class that represents servlet, its just used to show some data on web
 * application and is not used in this homework
 * 
 * @author Miroslav Filipovic
 */
public class Servlet extends HttpServlet {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html; charset=UTF-8");
		PrintWriter out = resp.getWriter();

		out.write("<html><body><h1>Naša prva web-stranica iz servlet</h1>");
		out.write("<p>Dobrodošli u našu prvu web stranicu!!!</p>");
		out.write("</body></html>");

	}

}
