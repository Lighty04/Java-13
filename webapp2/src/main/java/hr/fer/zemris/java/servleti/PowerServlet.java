package hr.fer.zemris.java.servleti;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * Class used to represent servlet which we use to make user and Excel file
 * which he can download with power of numbers from a to b with potention of i,
 * which is from 1 to n. Parameter a is minimal number that user can send and is
 * between -100 and 100 while b is maximal number, that can be between -100 and
 * 100 and we can caluclate then number powers from a to b, save it to Excel
 * file, and send it to user
 * 
 * @author Miroslav Filipovic
 */
@WebServlet(name = "powers", urlPatterns = { "/powers" })
public class PowerServlet extends HttpServlet {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		int aNumber = 0;
		int bNumber = 0;
		int nNumber = 0;

		String a = (String) req.getParameter("a");
		String b = (String) req.getParameter("b");
		String n = (String) req.getParameter("n");

		try {
			aNumber = Integer.parseInt(a);
			bNumber = Integer.parseInt(b);
			nNumber = Integer.parseInt(n);

			if (aNumber < -100 || aNumber > 100) {
				throw new Exception();
			}

			if (bNumber < -100 || bNumber > 100) {
				throw new Exception();
			}

			if (nNumber < 1 || nNumber > 5) {
				throw new Exception();
			}
		} catch (Exception e) {
			resp.setContentType("text/html");
			resp.getOutputStream()
					.write(("<html><body><h1>Parameters are invalid</h1><br/><p>a,bE[-100,100],nE[1,5]</p></body></html>")
							.getBytes());
			return;
		}

		try {
			resp.setContentType("application/ms-excel");
			resp.setHeader("Content-Disposition", "attachment; filename=testxls.xls");

			HSSFWorkbook wb = new HSSFWorkbook();
			HSSFSheet[] sheets = new HSSFSheet[nNumber];

			for (int i = 0; i < nNumber; i++) {
				sheets[i] = wb.createSheet();
			}

			for (int j = 0; j < nNumber; j++) {
				int number = aNumber;
				for (int i = 0; i <= bNumber - aNumber; i++) {
					HSSFRow row = sheets[j].createRow((short) i);
					row.createCell((short) 0).setCellValue(number);
					row.createCell((short) 1).setCellValue(Math.pow(number++, j + 1));
				}
			}

			ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
			wb.write(outByteStream);

			byte[] outArray = outByteStream.toByteArray();
			OutputStream outStream = resp.getOutputStream();
			outStream.write(outArray);
			outStream.flush();

		} catch (Exception ex) {
		}

	}

}
