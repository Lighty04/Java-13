package hr.fer.zemris.java.servleti;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.String;

/**
 * Class which represent servlet which is used to set some colour for
 * background. We can set red, green, cyan or white background over this
 * servelet. We send all data by ourselves without any *.jsp file
 * 
 * @author Miroslav Filipovic
 */
public class SetColorServelt extends HttpServlet {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		HttpSession session = req.getSession();

		resp.setContentType("text/html; charset=UTF-8");
		PrintWriter out = resp.getWriter();

		String color = (String) req.getParameter("color");
		session.setAttribute("color", color);

		if (color != null) {
			out.write("<html><body bgColor=\"" + color + "\">");
			out.write("<p>Color " + color + " is set!!!</p>");
		} else {
			out.write("<html><body>");
			out.write("<p>This color cannot be set!!!</p>");
		}
		out.write("</body><html>");
	}

}
