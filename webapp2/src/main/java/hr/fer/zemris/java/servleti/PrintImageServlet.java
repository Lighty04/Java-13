package hr.fer.zemris.java.servleti;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.util.Rotation;

import com.keypoint.PngEncoder;

/**
 * Class that is used to print out some simple data to user on web application.
 * We print out pie chart with three informations. It makes pie chart named Data
 * and represent simple example of how we can print out some data on web
 * application for user
 * 
 * @author Miroslav Filipovic
 */
public class PrintImageServlet extends HttpServlet {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		PieDataset dataset = createDataset();
		JFreeChart chart = createChart(dataset, "Data");

		resp.setContentType("image/png");

		BufferedImage buf = chart.createBufferedImage(640, 400, null);
		PngEncoder encoder = new PngEncoder(buf, false, 0, 9);
		resp.getOutputStream().write(encoder.pngEncode());
	}

	/**
	 * Method used to create all data that our chart needs so we can make new
	 * pie chart which we will print out for user
	 * 
	 * @param req
	 *            servlet request that we need for our result file
	 * @return PieDataset where is stored all data that chart needs
	 */
	private PieDataset createDataset() {
		DefaultPieDataset result = new DefaultPieDataset();
		result.setValue("Linux", 29);
		result.setValue("Mac", 20);
		result.setValue("Windows", 51);
		return result;

	}

	/**
	 * Method which is used to make new pie chart with data that we have sent
	 * over arguments
	 * 
	 * @param dataset
	 *            where is data stored
	 * @param title
	 *            how will chart call
	 * @return new instance of chart that we will show to user
	 */
	private JFreeChart createChart(PieDataset dataset, String title) {

		JFreeChart chart = ChartFactory.createPieChart3D(title, // chart title
				dataset, true, true, false);

		PiePlot3D plot = (PiePlot3D) chart.getPlot();
		plot.setStartAngle(290);
		plot.setDirection(Rotation.CLOCKWISE);
		plot.setForegroundAlpha(0.5f);
		return chart;

	}

}
