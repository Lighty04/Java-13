<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
	<body bgColor="${sessionScope.color}">
		<h1>
			Rezultati glasanja
		</h1>
		<p>
			Ovo su rezultat glasanja
		</p>
		<table border="1">
			<tr>
				<td align="center">
					Bend
				</td>
				<td align="center">
					Broj glasova	
				</td>
			</tr>
		<c:forEach var="bend" items="${bendiglasovi}">
	 	<tr>
			<td align="center">${bend.bend}</td>
			<td align="center">${bend.glasovi}</td>
		</tr>
		</c:forEach>
		</table>
		<h1>
			Grafički prikaz rezulata
		</h1>
		<p>
			<img src="crtaj-rezultate">
		</p>
		<h1>
			Rezultati u XLS formatu
		</h1>
		<p>
			Rezultati u XLS formatu dostupni su <a href="glasanje-xls">ovdje</a>
		</p>
		<h1>
			Razno
		</h1>
		<p>
			Primjeri pjesama pobjedničkih bendova:
		</p>
		<ul>
			<c:forEach var="pobjednik" items="${pobjednici}">
				<li>
					<a href="${pobjednik.url}">${pobjednik.bend}</a>
				</li>
			</c:forEach>
		</ul>
	</body>
</html>