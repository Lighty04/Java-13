<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
	<body bgColor="${sessionScope.color}">
		<table border="1">
		  <tr>
		  	<th>Sinus</th>
		    <c:forEach var="sinResult" items="${sin}">
			<td>${sinResult}</td>
			</c:forEach>
		  </tr>
		  <tr>
		    <th>Cosine</th>
		    <c:forEach var="cosResult" items="${cos}">
			<td>${cosResult}</td>
			</c:forEach>
		  </tr>
		</table>
	</body>
</html>
