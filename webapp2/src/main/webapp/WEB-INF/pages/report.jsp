<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
	<body bgColor="${sessionScope.color}">
		<h1>
			OS usage
		</h1>
		<p>
			Here are the results of OS usage in survey that we completed
		</p>
		<p>
			<img src="printImage">
		</p>
	</body>
</html>