<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
	<body bgColor="${sessionScope.color}">
		<p>Choose color:</p>
			<a href="/webapp2/setcolor?color=white">White</a>
			<a href="/webapp2/setcolor?color=red">Red</a>
			<a href="/webapp2/setcolor?color=green">Green</a>
			<a href="/webapp2/setcolor?color=cyan">Cyan</a>
	</body>
</html>