<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<html>
	<body bgColor="${sessionScope.color}">
		<a href="colors.jsp">Background color chooser</a>
		<br/>
		<a href="trigonometric?a=0&b=90">Trigonometric results</a>
		<br/>
		<a href="funny">Funny story</a>
		<br/>
		<a href="reportImage">Report</a>
		<br/>
		<a href="powers?a=1&b=100&n=3">Powers</a>
		<br/>
		<a href="appinfo.jsp">Application informations</a>
	</body>
</html>
