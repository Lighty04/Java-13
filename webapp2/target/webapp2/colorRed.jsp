<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:useBean id="pickedBgCol" class="hr.fer.zemris.java.servleti.Color" scope="request" />
<jsp:setProperty property="color" name="pickedBgCol" value="red" />


<html>
<body bgcolor="${pickedBgCol.color}">
<p>You set color to ${pickedBgCol.color}</p>
</body>
</html>